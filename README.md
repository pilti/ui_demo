# UI_Demo

Demo project for UI Selenium Automation

This is a Demo project for UI Automation - Web TransferWise.

Single Feature file with 3 scenarios for few currency conversion. This feature files are based on the Karate DSL extended 
for UI automation (pageObject pattern).

Path to the feature file : /src/test/java/Demo/BDD_TestScripts/transferwise/currencyConversion.feature
Runner to execute this file : /src/test/java/Demo/BDD_TestScripts/transferwise/Runner_Test.java

Feature file can be run directly, but if the report is generated only if run through runner file.

Report Path: /target/cucumber-html-reports/overview-features.html.

Please have a try and let me know your comments at idealumesh@gmail.com
